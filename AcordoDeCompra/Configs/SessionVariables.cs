﻿namespace AcordoDeCompra.Configs {
    public class SessionVariables {
        public class Variaveis {
            public string Nome { get; set; }

            public static Variaveis AcordoCompra = new Variaveis() { Nome = "acordocompralist" };
        }
    }
}