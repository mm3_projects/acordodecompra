﻿using AcordoDeCompra.Configs;
using AcordoDeCompra.Logic;
using AcordoDeCompra.Models;
using PolisportAPI;
using PolisportAPI.ModeloEstruturas.Portal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AcordoDeCompra.Controllers {
    public class HomeController : Controller {
        #region dependencies
        AcordoDeCompraLogic acordoDeCompraLogic = new AcordoDeCompraLogic();
        #endregion

        [HttpGet]
        public ActionResult Index (string sortBy) {
            if (Session[SessionVariables.Variaveis.AcordoCompra.Nome] != null) {
                var data = (AcordoDeCompraModel)Session[SessionVariables.Variaveis.AcordoCompra.Nome];
                data.SortBy = sortBy;
                data.artigos = acordoDeCompraLogic.GetSortBy(sortBy, data.artigos);

                return View(data);
            }

            var acordoCompraModel = new AcordoDeCompraModel();
            var result = acordoDeCompraLogic.GetInfo();

            if (result == null) {
                return View(new AcordoDeCompraModel() {
                    artigos = new List<AcordoDeCompraPoco>(),
                    Sucesso = string.Empty,
                    Error = "Não foi possível carregar informação! Contacte o administrador!"
                });
            }

            if (result.Count == 0) {
                return View(new AcordoDeCompraModel() {
                    artigos = new List<AcordoDeCompraPoco>(),
                    Sucesso = string.Empty,
                    Error = "Não há dados para processar!!"
                });
            }

            acordoCompraModel = new AcordoDeCompraModel() {
                artigos = acordoDeCompraLogic.GetSortBy(sortBy, result),
                Error = string.Empty,
                Sucesso = string.Empty,
                SortBy = "Cotacao"
            };

            Session[SessionVariables.Variaveis.AcordoCompra.Nome] = acordoCompraModel;
            return View(acordoCompraModel);
        }

        [HttpPost]
        public ActionResult Gravar (string cotacaoid, string artigo, string metodorecepcao, string embalamento,
            string fornecimento, string estado, string codigo, string servico, string processoservico, string groupType,
            string responsavel, string armazem, string newitem, string milheiro) {
            var list = (AcordoDeCompraModel)Session[SessionVariables.Variaveis.AcordoCompra.Nome];

            var poco = list.artigos
                .Where(x => x.CotacaoID.ToLower().Trim() == cotacaoid.ToLower().Trim()
                && x.Artigo.ToLower().Trim() == artigo.ToLower().Trim())
                .FirstOrDefault();

            poco.MetodoRecepcao = metodorecepcao;
            poco.Embalamento = embalamento;
            //poco.FornecimentoMultiplo = fornecimento;
            poco.Estado = estado;
            poco.Servico = servico;
            poco.ProcessoServico = processoservico;
            poco.GroupType = groupType;
            poco.Responsavel = responsavel;
            poco.Artigo = !string.IsNullOrEmpty(newitem) ? newitem.Trim() : poco.Artigo;
            poco.Milheiro = int.Parse(milheiro);
            poco.Armazem = armazem;

            var m3Request = acordoDeCompraLogic.CriarAcordoDeCompraRequest(poco);

            if (string.IsNullOrEmpty(m3Request)) {
                try {
                    acordoDeCompraLogic.Update(poco.CotacaoID, poco.Artigo);
                } catch (Exception ex) {
                    PolisportAPI.API.SendEmail("Erro ao alterar estado do artigo:" + artigo + ";cotacao:" + cotacaoid, new string[] { SystemConfigs.EmailErros }, new string[] { }, "UpdateCotacaoLinhasStatus - BDI:" + ex.ToString());
                }

                list.artigos.Remove(poco);
                var data = (AcordoDeCompraModel)Session[SessionVariables.Variaveis.AcordoCompra.Nome];
                data.Sucesso = "Acordo de compra criado";
                data.Error = string.Empty;
                data.artigos = list.artigos;

                Session[SessionVariables.Variaveis.AcordoCompra.Nome] = data;
                return RedirectToAction("Index");
            } else {
                var data = (AcordoDeCompraModel)Session[SessionVariables.Variaveis.AcordoCompra.Nome];
                data.Error = "Erro ao enviar para o M3:" + m3Request;
                data.Sucesso = string.Empty;
                data.artigos = list.artigos;
                data.SortBy = list.SortBy;
                Session[SessionVariables.Variaveis.AcordoCompra.Nome] = data;
                return RedirectToAction("Index");
            }
        }


        [HttpGet]
        public ActionResult Cancelar (string cotacaoid, string artigo) {
            var list = new AcordoDeCompraModel();
            var data = (AcordoDeCompraModel)Session[SessionVariables.Variaveis.AcordoCompra.Nome];

            try {
                list = (AcordoDeCompraModel)Session[SessionVariables.Variaveis.AcordoCompra.Nome];
                var poco = list.artigos
                    .Where(x => x.CotacaoID.ToLower().Trim() == cotacaoid.ToLower().Trim()
                    && x.Artigo.ToLower().Trim() == artigo.ToLower().Trim())
                    .FirstOrDefault();

                list.artigos.Remove(poco);
                acordoDeCompraLogic.Update(poco.CotacaoID, poco.Artigo);

                data.Sucesso = "Artigo cancelado!";
                data.Error = string.Empty;
            } catch (Exception) {
                data.Sucesso = string.Empty;
                data.Error = "Erro ao cancelar artigo de cotacao. CotacaoID:" + cotacaoid + ";Artigo:" + artigo + "!Contacte o administrador";
            }

            data.artigos = list.artigos;
            Session[SessionVariables.Variaveis.AcordoCompra.Nome] = data;
            return RedirectToAction("Index");
        }
    }
}