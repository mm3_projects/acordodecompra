﻿using System.Web.Mvc;

namespace AcordoDeCompra {
    public class FilterConfig {
        public static void RegisterGlobalFilters (GlobalFilterCollection filters) {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
