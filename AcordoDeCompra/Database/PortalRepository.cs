﻿using System.Data;
using System.Data.SqlClient;

namespace AcordoDeCompra.Database.Repositories {
    public class PortalRepository {
        public DataTable GetCotacao (SqlConnection conn) {
            var query = @"with 
                         d1 as (SELECT CotacaoID, FornecedorCode, max(DataCriacao) DataCriacao,  Artigo, Quantidade,UrlSite ,NomeFornecedor 
                         FROM V_COTACAO 
                         WHERE IPA=1 
                         group by CotacaoID, FornecedorCode, Artigo, Quantidade,UrlSite,NomeFornecedor),
                         d2 as (
                         SELECT sum(PrecoTotal) as Total 
                         FROM V_COTACAO 
                         WHERE CotacaoEst=1)

                         select d0.*,d2.* 
                         from V_COTACAO d0 
                         inner join d1 on d0.CotacaoID=d1.CotacaoID and d0.FornecedorCode=d1.FornecedorCode and d0.Artigo=d1.Artigo and d0.Quantidade=d1.Quantidade 
                         and d0.DataCriacao=d1.DataCriacao 
                         full join d2 on 1=1 
                         where d0.IPA=1 and CotacaoEst=1 and d0.Processado=0
                         order by d0.Artigo,d0.FornecedorCode, cast(d0.Quantidade as float)";

            return PolisportAPI.Data.Database.QueryReader(query, "GetCotacao", conn);
        }

        public void Update (SqlConnection conn, string cotacaoID, string item) {
            var query = string.Format(@"UPDATE [dbo].[COTACAO_LINHAS]
                        SET Processado=1
                        Where CotacaoID='{0}' and Artigo='{1}'", cotacaoID, item);

            PolisportAPI.Data.Database.ExecuteNonQuery(query, conn);
        }
    }
}