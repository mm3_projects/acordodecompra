﻿using System.Data;
using System.Data.SqlClient;

namespace AcordoDeCompra.Database.Repositories {
    public class M3Repository {
        public DataTable CheckQMS (SqlConnection conn, string artigo) {
            var query = string.Format(@"select MMQMGP from
                        MVXJDTA.MITMAS
                        WHERE MMITNO='{0}'", artigo);

            return PolisportAPI.Data.Database.QueryReader(query, "CheckQMS", conn);
        }
    }
}