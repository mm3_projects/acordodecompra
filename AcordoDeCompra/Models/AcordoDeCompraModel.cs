﻿using PolisportAPI.ModeloEstruturas.Portal;
using System.Collections.Generic;

namespace AcordoDeCompra.Models {
    public class AcordoDeCompraModel {
        public List<AcordoDeCompraPoco> artigos { get; set; }

        public string Error { get; set; }

        public string Sucesso { get; set; }

        public string SortBy { get; set; }
    }
}