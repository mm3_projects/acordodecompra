﻿using AcordoDeCompra.Database.Repositories;
using PolisportAPI;
using PolisportAPI.ModeloEstruturas.Portal;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace AcordoDeCompra.Logic {
    public class AcordoDeCompraLogic {
        #region dependencies
        M3Repository m3Repo = new M3Repository();
        PortalRepository portalRepo = new PortalRepository();
        #endregion

        public List<AcordoDeCompraPoco> GetSortBy (string sortBy, List<AcordoDeCompraPoco> result) {
            if (!string.IsNullOrEmpty(sortBy)) {
                if (sortBy.ToLower() == "cotacao") {
                    result = result.OrderBy(x => x.CotacaoID).ToList();
                } else if (sortBy.ToLower() == "artigo") {
                    result = result.OrderBy(x => x.Artigo).ToList();
                } else if (sortBy.ToLower() == "fornecedor") {
                    result = result.OrderBy(x => x.NumFornecedor).ToList();
                }
            } else {
                result = result.OrderBy(x => x.CotacaoID).ToList();
            }

            return result;
        }


        public List<AcordoDeCompraPoco> GetInfo () {
            try {
                var cotacaoInfo = GetCotacao();

                if (cotacaoInfo.Rows.Count == 0) {
                    return new List<AcordoDeCompraPoco>();
                }

                var listInfo = new List<AcordoDeCompraPoco>();

                foreach (DataRow cotacao in cotacaoInfo.Rows) {
                    if (cotacao["Comparacao"].ToString() == "1") {
                        var checkQMS = CheckQMS(cotacao["Artigo"].ToString());
                        listInfo.Add(new AcordoDeCompraPoco() {
                            Artigo = cotacao["Artigo"].ToString(),
                            Company = 1,
                            CotacaoID = cotacao["CotacaoID"].ToString(),
                            PrecoCompra = decimal.Parse(cotacao["PrecoUnitario"].ToString()),
                            NumFornecedor = cotacao["FornecedorCode"].ToString(),
                            Quantidade = int.Parse(cotacao["Quantidade"].ToString()),
                            ValidFrom = DateTime.Now,
                            Embalamento = "0000",
                            Estado = "06",
                            ProcessoServico = "001",
                            NivelCQ = string.IsNullOrEmpty(checkQMS) ? "4" : "1",
                            InspectionText = string.IsNullOrEmpty(checkQMS) ? "." : checkQMS,
                            EscalaoPrincipal = cotacao["Comparacao"].ToString() == "1" ? true : false,
                            PrazoAbastecimento = int.Parse(cotacao["PrazoEntrega"].ToString()),
                            Escaloes = cotacaoInfo
                            .Rows
                            .OfType<DataRow>()
                            .Where(x =>
                                x["Comparacao"].ToString() == "0" &&
                                x["FornecedorCode"].ToString() == cotacao["FornecedorCode"].ToString() &&
                                x["Artigo"].ToString() == cotacao["Artigo"].ToString())
                            .ToDictionary(x => int.Parse(x["Quantidade"].ToString()), x => decimal.Parse(x["PrecoUnitario"].ToString()))
                        });
                    }
                }

                return listInfo;
            } catch (Exception) {
                return null;
            }
        }

        private string CheckQMS (string artigo) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(PolisportAPI.SystemConfigs.LigacaoM3)) {
                var resultado = m3Repo.CheckQMS(conn, artigo);

                if (resultado.Rows.Count == 0) {
                    return string.Empty;
                }

                if (string.IsNullOrEmpty(resultado.Rows[0][0].ToString().Trim())) {
                    return string.Empty;
                } else {
                    return "qms";
                }
            }
        }

        private DataTable GetCotacao () {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.ConnStringPortal)) {
                return portalRepo.GetCotacao(conn);
            }
        }

        public void Update (string cotacaoID, string item) {
            using (var conn = PolisportAPI.Data.Database.OpenConnection(SystemConfigs.ConnStringPortal)) {
                portalRepo.Update(conn, cotacaoID, item);
            }
        }

        public string CriarAcordoDeCompraRequest (AcordoDeCompraPoco poco) {
            try {
                var webResponse = PolisportAPI.EconnetorInvoke.EndpoinConnector<AcordoDeCompraPoco>(EconnetorInvoke.EnderecosPedidos.CriarAcordoDeCompra, poco);
                var response = string.Empty;
                var encoding = Encoding.UTF8;

                using (var reader = new StreamReader(webResponse.GetResponseStream(), encoding)) {
                    response = reader.ReadToEnd();
                }

                return response.Replace("\r\n", "");
            } catch (Exception ex) {
                return ex.ToString();
            }
        }
    }
}